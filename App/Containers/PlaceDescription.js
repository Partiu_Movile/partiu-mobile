import React, { Component } from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import StarRating from 'react-native-star-rating';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import {
    Button,
    Image,
    TouchableOpacity,
    Text,
    ScrollView,
    View
} from 'react-native';

// Components
import Back from '../Components/Back'
import Ionicons from 'react-native-vector-icons/Ionicons'

// Styles
import { Metrics } from '../Themes/'
import styles from './Styles/PlaceDescription'

export default class PlaceDescription extends Component {

  static navigationOptions = {
    header: null
  }

    constructor () {
      super()

      this.state = {
        activeSlide: 0,
      }
    }

    componentDidMount() {
      console.warn(this.props.navigation.getParam('place', {}));
    }

    _handleWish = () => {
      const place = this.props.navigation.getParam('place', {})

      this.setState({
        place: {
          ...place,
          wish: !place.wish
        }
      })
    }

    _handleFavorite = () => {
      const place = this.props.navigation.getParam('place', {})

      this.setState({
        place: {
          ...place,
          favorite: !place.favorite
        }
      })
    }

    _handleBack = () => {
      this.props.navigation.goBack();
    }

    _getOperation = () => {
      const place = this.props.navigation.getParam('place', {})

      return place.operation.map(function(item) {
        return (
          <Text key={item.id} style={styles.operationText}>{item.description}</Text>
        )
      });
    }

    _renderCarouselItem = ({item, index}) => {
      return (
        <Image style={styles.image} source={item.image} />
      );
    }

    _getCarouselPagination() {
      const { activeSlide } = this.state;
      const place = this.props.navigation.getParam('place', {})

      return (
        <Pagination
          dotsLength={place.images.length}
          activeDotIndex={activeSlide}
          containerStyle={styles.pagination}
          dotStyle={styles.paginationDots}
          inactiveDotOpacity={0.5}
          inactiveDotScale={1}
        />
      );
    }

    _onPressMenu = () => {
      // this.props.navigation.navigate('Menu')
    }

    render () {
        const { activeSlide } = this.state;
        const place = this.props.navigation.getParam('place', {})

        return (
            <View>
              <ScrollView style={styles.scrollview}>
                <View style={styles.container}>
                  <Back goBack={this._handleBack.bind(this)}/>

                  <View style={styles.row}>
                    <Text style={styles.name}>{place.name}</Text>

                    <TouchableOpacity onPress={this._handleWish}>
                      {
                        place.wish
                        ? <MaterialIcons name="playlist-add-check" size={22} style={styles.wish} />
                        : <MaterialIcons name="playlist-add" size={22} style={styles.wish} />
                      }
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this._handleFavorite}>
                      {
                        place.favorite
                        ? <MaterialIcons name="favorite" size={22} style={styles.favorite} />
                        : <MaterialIcons name="favorite-border" size={22} style={styles.favorite} />
                      }
                    </TouchableOpacity>
                  </View>

                  <View style={styles.row}>
                    <StarRating
                      starSize={20}
                      fullStarColor={'#FFC700'}
                      emptyStarColor={'#FFC700'}
                      disabled={true}
                      maxStars={5}
                      rating={place.rate}
                    />

                    <View style={styles.recommended}>
                      <MaterialIcons name={place.recommended.icon} size={26} style={styles.recommendedIcon} />
                      <Text style={styles.recommendedText}>{place.recommended.title}</Text>
                    </View>
                  </View>

                  <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                    <View style={styles.flex}>
                      <Text style={styles.label}>Status</Text>
                      <Text style={[styles.status, place.status ? { color: '#25AE88' } : { color: '#FF0000' }]}>{place.status ? 'Aberto' : 'Fechado'}</Text>
                    </View>

                    <View style={styles.flex}>
                      <Text style={styles.label}>Preço</Text>
                      <StarRating
                        starSize={15}
                        fullStarColor={'#03AFBA'}
                        emptyStarColor={'#e5e5e5'}
                        fullStar={'attach-money'}
                        emptyStar={'attach-money'}
                        iconSet={'MaterialIcons'}
                        disabled={true}
                        maxStars={5}
                        rating={place.price}
                      />
                    </View>

                    <View style={styles.flex}>
                      <Text style={styles.label}>Movimento</Text>
                      <Text style={styles.flux}>{place.flux}</Text>
                    </View>
                  </View>

                  <View style={[styles.row, { justifyContent: 'flex-start' }]}>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={this._onPressMenu}
                      underlayColor='#fff'>
                        <Text style={styles.buttonText}>ver cardápio</Text>
                     </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.row}>
                  <Carousel
                    ref={ref => this.Carousel = ref}
                    onSnapToItem={index => this.setState({ activeSlide: index }) }
                    data={place.images}
                    renderItem={this._renderCarouselItem.bind(this)}
                    sliderWidth={Metrics.screenWidth}
                    itemWidth={Metrics.screenWidth}
                    enableMomentum={false}
                    inactiveSlideScale={1}
                    activeSlideOffset={1}
                    swipeThreshold={0}
                    removeClippedSubviews={true}
                    loop={true}
                    autoplay={true}
                    autoplayDelay={2500}
                    autoplayInterval={5000}
                  />
                  { this._getCarouselPagination() }
                </View>

                <View style={styles.container}>
                  <View style={styles.row}>
                    <Text style={styles.description}>{place.description}</Text>
                  </View>
                </View>

                <View style={styles.operation}>
                  <Text style={styles.operationTitle}>Atendimento</Text>
                  { this._getOperation() }
                </View>

                <View style={styles.container}>
                  <View style={styles.row}>
                    <SimpleLineIcons name="location-pin" size={22} style={styles.locationIcon} />
                    <Text style={styles.locationText}>{place.address}</Text>
                  </View>
                </View>
              </ScrollView>

              <View style={styles.checkinContainer}>
                <TouchableOpacity
                  onPress={this._onPressCheckin}
                  underlayColor='#fff'>
                    <Text style={styles.checkinText}>ir nesse restaurante</Text>
                 </TouchableOpacity>
              </View>
             </View>
        );
    }
}
