import React, { Component } from 'react';

import {
    View,
    ScrollView,
    Text,
    Image,
    StyleSheet,
    TextInput,
    FlatList,
    TouchableOpacity
} from 'react-native';

import Card from '../Components/Card';

import Carousel from 'react-native-snap-carousel';

export default class Feed extends Component {
    static navigationOptions = {
        header: null
    }

    constructor() {
        super();
        this.state = {
            places: []
        }
    }

    componentWillMount() {
        fetch('http://192.168.25.216:8000/feed', {
            method: 'GET',
        })
            .then(resposta => resposta.json())
            .then(async (json) => {
                this.setState({ places: json.feed });
            })
            .catch(err => {
                this.setState({ places: [] });
            })
    }

    _renderItem({ item, index }) {
        return (
            <View>
                <Image source={require('../../assets/img/rest.jpg')} resizeMode='contain' />
            </View>
        );
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.header}>
                    
                    <View style={[styles.sliderView,{width:'100%'}]}> 
                        <Image source={require('../../assets/img/banner_.png')} 
                                style={styles.imageView} /> 
                    </View>    

                    <Text style={styles.welcomeText}>Experiências para você</Text>

                    <TextInput
                        style={styles.input}
                        onChangeText={(search) => this.setState({ search })}
                        value={this.state.search}
                        keyboardAppearance='light'
                        placeholder='Search...'
                        autoCapitalize='none'
                        autoCorrect={false}
                        returnKeyType="next"
                        blurOnSubmit={false}
                    />
                </View>
                <View>
                    <FlatList
                        extraData={this.state}
                        data={this.state.places}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('PlaceDescription', {
                                place: item
                            })}>
                            <Card item={item} />
                            </TouchableOpacity>
                        )}
                        keyExtractor={item => `${item.id}`}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        padding: 20
    },
    welcomeText: {
        fontSize: 28,
        fontFamily: "Avenir",
        fontWeight: "100",
        color: '#03AFBA',
        textAlign: 'center', 
        paddingTop: 10,
        paddingBottom: 20,
    },
    header: {
        marginBottom: 20
    },
    sliderView: {
        height: 400,
        width: 450,
        paddingLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageView: {
        height: 350,
        width: 450,
        justifyContent: 'center',
        alignItems: 'center',
    },
})