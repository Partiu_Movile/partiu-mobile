import React, { Component } from 'react';

import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    Image,
    TextInput,
    AsyncStorage,
    Button
} from 'react-native';

export default class Login extends Component {
    static navigationOptions = {
        header: null
    }

    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            authFailed: false
        }
    }

    async login() {
        const data = {
            email: this.state.username,
            password: this.state.password
        }

        fetch('http://192.168.25.216:8000/authUser/authenticate', {
            method: 'POST',
            body: JSON.stringify(data),
        })
            .then(resposta => resposta.json())
            .then(async (json) => {
                await AsyncStorage.setItem('accessToken', json.token);
                this.setState({ authFailed: false });
                this.props.navigation.navigate('App');
            })
            .catch(err => {
                this.setState({ authFailed: true });
            })
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.logo} source={require("../../assets/img/logo_partiu.png")} resizeMode='contain' />
                </View>
                <View style={styles.body}>
                    <TextInput
                        style={styles.input}
                        onChangeText={(username) => this.setState({ username })}
                        value={this.state.username}
                        keyboardAppearance='light'
                        placeholder='Username'
                        autoFocus={true}
                        autoCapitalize='none'
                        autoCorrect={false}
                        keyboardType="default"
                        returnKeyType="next"
                        blurOnSubmit={false}
                    />
                    <TextInput
                        style={styles.input}
                        onChangeText={(password) => this.setState({ password })}
                        value={this.state.password}
                        keyboardAppearance='light'
                        placeholder='Password'
                        autoCapitalize='none'
                        autoCorrect={false}
                        returnKeyType="next"
                        blurOnSubmit={false}
                        secureTextEntry={true}
                    />
                    <Button
                        onPress={() => this.login()}
                        title="Login"
                        color="#1ab4be"
                        accessibilityLabel="Login"
                    />
                    <View style={styles.forgotPassword}>
                        <Text onPress={() => this.props.navigation.navigate('ForgotPassword')}>Forgot your password?</Text>
                    </View>
                </View>
                <View style={styles.footer}>
                    <View>
                        <Text onPress={() => this.props.navigation.navigate('CreateAccount')}>Click here to create a new user!</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    header: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 70
    },
    logo: {
        height: 100,
    },
    body: {
        marginTop: 20,
        marginLeft: 30,
        marginRight: 30
    },
    input: {
        marginBottom: 10,
        height: 40
    },
    forgotPassword: {
        alignItems: 'center',
        marginTop: 30
    },
    footer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 80
    }
});