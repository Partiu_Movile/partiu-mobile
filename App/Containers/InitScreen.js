import React, { Component } from 'react';
import { View, AsyncStorage, StyleSheet } from 'react-native';

import Spinner from 'react-native-spinkit';

export default class LoadingScreen extends Component {

    constructor() {
        super();
        this.isTokenValid();
    }

    isTokenValid = async () => {
        const accessToken = await AsyncStorage.getItem('accessToken');

        let redirectTo = accessToken ? 'App' : 'Auth';

        this.props.navigation.navigate(redirectTo);
    };

    render() {
        return (
            <View style={styles.container}>
                <Spinner isVisible={true} size={100} type={'9CubeGrid'} color={'gray'} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    }
});