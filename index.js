import './App/Config/ReactotronConfig'
import { AppRegistry, YellowBox } from 'react-native'
import App from './App/Containers/App'

AppRegistry.registerComponent('partiu', () => App)

// Ignored warnings:
YellowBox.ignoreWarnings(
  [
    'Warning: isMounted(...) is deprecated',
    'Warning: componentWillMount is deprecated',
    'Warning: componentWillReceiveProps is deprecated',
    'Warning: componentWillUpdate is deprecated',
    // https://github.com/facebook/react-native/issues/17504
    'Module RCTImageLoader',
    // https://github.com/facebook/react-native/issues/18201
    'Class RCTCxxModule',
  ]
);
