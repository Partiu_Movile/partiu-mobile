import React, { Component } from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import styles from './Styles/Back'

export default class Back extends Component {

  _handleBack = () => {
    this.props.goBack();
  }

  render () {
      return (
        <View style={styles.container}>
          <TouchableOpacity onPress={this._handleBack}>
            <View style={styles.content}>
              <Ionicons name="ios-arrow-back" size={30} style={styles.icon} />
              <Text style={styles.text}>Voltar</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
  }
}
