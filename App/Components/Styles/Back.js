import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15
  },
  icon: {
    color: Colors.black,
    marginRight: 10
  },
  text: {
    fontSize: Fonts.size.regular
  },
})
