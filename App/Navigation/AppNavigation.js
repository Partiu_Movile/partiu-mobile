import React from 'react';
import { SwitchNavigator, StackNavigator, DrawerNavigator, TabNavigator, TabBarBottom } from 'react-navigation';

import InitScreen from '../Containers/InitScreen';
import Feed from '../Containers/Feed';
import PlaceDescription from '../Containers/PlaceDescription';
import FoodDescription from '../Containers/FoodDescription';
import Cart from '../Containers/Cart';
import Menu from '../Containers/Menu';
import Bill from '../Containers/Bill';
import Review from '../Containers/Review';
import Login from '../Containers/Login';
import CreateAccount from '../Containers/CreateAccount';
import ForgotPassword from '../Containers/ForgotPassword';
import SideMenu from '../Containers/SideMenu';

import GuyProfile from '../Containers/GuyProfile';
import GuysFeed from '../Containers/GuysFeed';
import Search from '../Containers/Search';
import ExperienceDescription from '../Containers/ExperienceDescription';

import MyList from '../Containers/MyList';

import MyProfile from '../Containers/MyProfile';

import PendentsReviews from '../Containers/PendentsReviews';

import Config from '../Containers/Config';

import Followers from '../Containers/Followers';
import Following from '../Containers/Following';

import SplitBill from '../Containers/SplitBill';

import styles from './Styles/NavigationStyles';

import Icon from 'react-native-vector-icons/FontAwesome';

const ExploreStack = StackNavigator({
    Feed: Feed,
    PlaceDescription: PlaceDescription,
    Menu: Menu,
    FoodDescription: FoodDescription,
    SplitBill: SplitBill,
    Cart: Cart,
    Bill: Bill,
    Review: Review
});

const GuysStack = StackNavigator({
    GuysFeed: GuysFeed,
    Search: Search,
    GuyProfile: GuyProfile,
    ExperienceDescription: ExperienceDescription
});

const ListStack = StackNavigator({
    MyList: MyList,
    PlaceDescription: PlaceDescription,
    Menu: Menu,
    FoodDescription: FoodDescription,
    Cart: Cart,
    Bill: Bill,
    SplitBill:SplitBill,
    Review: Review
});

const ProfileStack = StackNavigator({
    MyProfile: MyProfile,
    PendentsReviews: PendentsReviews,
    Config: Config,
    Followers: Followers,
    Following: Following
});

const AppStack = TabNavigator({
    Explore: ExploreStack,
    Guys: GuysStack,
    List: ListStack,
    Profile: ProfileStack
},
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Explore') {
                    iconName = 'globe';
                } 
                else if (routeName === 'Guys') {
                    iconName = 'users';
                }
                else if (routeName === 'List') {
                    iconName = 'search';
                }
                else if (routeName === 'Profile') {
                    iconName =  'user';
                }

                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Icon name={iconName} size={25} color='gray' />;
            },
        }),
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
        tabBarOptions: {
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
        },
        animationEnabled: false,
        swipeEnabled: false,
    });

const MainStack = DrawerNavigator(
    {
        Workspace: AppStack
    },
    {
        contentComponent: SideMenu,
        drawerWidth: 300
    }
);

const AuthStack = StackNavigator({
    Login: Login,
    CreateAccount: CreateAccount,
    ForgotPassword: ForgotPassword
});

export default SwitchNavigator(
    {
        Init: InitScreen,
        App: MainStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'Init',
        headerMode: 'none',
        navigationOptions: {
            headerStyle: styles.header
        }
    }
);