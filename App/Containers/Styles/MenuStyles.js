import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    oldValue: {
        textDecorationLine: 'line-through',
    },
    itemTitle: {
        fontSize: 22,
        fontFamily: "Avenir",
    },
    itemDetail: {
        fontSize: 18,
        fontFamily: "Avenir",
    },
    sectionTitle: {
        fontSize: 28,
        fontFamily: "Avenir",
        paddingTop: 30,
        paddingLeft: 15,
    },
    imageView: {
        width: '30%',
        height: 100 ,
        margin: 7,
        borderRadius : 7
    },
    floatContainer: {
        flex:1, 
        flexDirection: 'row', 
        padding: 15
    },
    floatContainerNoPadding: {
        flex:1, 
        flexDirection: 'row'
    },
    headerSubTitle: {
        paddingLeft: 15,
    },
    headerSubTitleBallon: {
        padding: 10,
    },
    headerTitle: {
        fontSize: 30,
        padding: 15
    },
    oval: {
        height: 50,
        width: 165,
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        backgroundColor: 'lightgray',
        paddingLeft: 10
    },
    bottomOval: {
        height: 90,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        backgroundColor: '#FFCC16'
    },
})
