import React, { Component } from 'react';

import {
    View,
    TextInput,

} from 'react-native';

export default class Review extends Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 1
        };
    }
    
    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    sendReview() {

    }

    render() {
        return (
            <View>
                <TextInput style={styles.input}
                    onChangeText={(review) => this.setState({ review })}
                    value={this.state.review}
                    keyboardAppearance='light'
                    placeholder='Review...'
                />
                <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={this.state.starCount}
                    selectedStar={(rating) => this.onStarRatingPress(rating)}
                />
                <Button
                    onPress={() => this.sendReview()}
                    title="Login"
                    color="#841584"
                    accessibilityLabel="Login"
                />
            </View>
        );
    }
}