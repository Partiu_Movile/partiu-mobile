import React, { Component } from 'react';
import {
    View,
    Image,
    Text,
    StyleSheet
} from 'react-native';

export default class Card extends Component {
    render() {
        const { item } = this.props;
        return (
            <View style={styles.oval}>
                <View styles={styles.floatContainer}>
                    <Image style={styles.img} source={require('../../assets/img/food.jpeg')} resizeMode='contain' />
                    <Text style={styles.itemName}>{item.name}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginLeft: 30,
    },
    img: {
        height: 120,
        width: 120
    },
    floatContainer: {
        flex:1, 
        flexDirection: 'row', 
        padding: 15
    },
    oval: {
        height: 150,
        width: '100%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        backgroundColor: '#F2F2F2',
        paddingLeft: 10
    },
    itemName: {
        color: '#03AFBA',
    }
})