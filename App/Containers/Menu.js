import React, { Component } from 'react';

import {
    View,
    Text,
    ListView,
    FlatList,
    Image,
    ScrollView,
    TouchableOpacity,
    Button,
} from 'react-native';

// Styles
import styles from './Styles/MenuStyles'
import Back from '../Components/Back'
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class Menu extends Component {

    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            promoDataSource: [ 
              {key: 0, title: '969 - Bolinho de salmão', detail:'4 unidades deliciosas trouxinhas crocantes de salmão', defaultValue:'19.90', promotionValue:'12.90', uri:'https://www.freeiconspng.com/uploads/fast-food-free-food-icons-28.png'}, 
              {key: 1, title: '970 - Bolinho de sushi', detail:'5 unidades deliciosas trouxinhas crocantes de salmão', defaultValue:'19.95', promotionValue:'12.95', uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRinFa2ouYZ5CRFwczt9q4pVrmpuLkC796Ar75PBHSF9WexOw9J'},
              {key: 2, title: '971 - Bolinho de camarão', detail:'6 unidades deliciosas trouxinhas crocantes de salmão', defaultValue:'19.95', promotionValue:'12.95', uri:'https://cdn.pixabay.com/photo/2017/02/01/00/02/food-2028372_960_720.png'},
            ],
            platesDataSource: [ 
                {key: 0, title: '969 - Bolinho de salmão', detail:'4 unidades deliciosas trouxinhas crocantes de salmão', defaultValue:'19.90', promotionValue:'12.90', uri:'https://www.freeiconspng.com/uploads/fast-food-free-food-icons-28.png'}, 
                {key: 1, title: '970 - Bolinho de sushi', detail:'5 unidades deliciosas trouxinhas crocantes de salmão', defaultValue:'19.95', promotionValue:'12.95', uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRinFa2ouYZ5CRFwczt9q4pVrmpuLkC796Ar75PBHSF9WexOw9J'},
                {key: 2, title: '971 - Bolinho de camarão', detail:'6 unidades deliciosas trouxinhas crocantes de salmão', defaultValue:'19.95', promotionValue:'12.95', uri:'https://cdn.pixabay.com/photo/2017/02/01/00/02/food-2028372_960_720.png'},
              ],
        };
    }

    render () {
        return (
            <ScrollView style={{paddingTop: 20}}>
                
                <View style={[styles.container, {paddingLeft: 20}]}>
                    <TouchableOpacity onPress={this._handleBack}>
                        <View style={styles.content}>
                        <Ionicons name="ios-arrow-down" size={30} style={styles.icon} />
                        </View>
                    </TouchableOpacity>
                </View>

                <Text style={[styles.headerTitle]}>Bem vindo ao{"\n"}Si Senor Pinheiros!</Text>

                <Text style={[styles.headerSubTitle, styles.itemDetail]}>Você está dividindo com: </Text>
                <View style={[styles.floatContainer, {justifyContent:'space-between'}]}>
                    <View style={styles.oval}>
                        <View style={styles.floatContainerNoPadding}>
                            <Text style={[styles.headerSubTitleBallon, styles.itemDetail]}>Luciana Lana</Text>
                            <Text style={{fontWeight:'bold', paddingTop: 13}}>x</Text>
                        </View>
                    </View>

                    <View style={[styles.oval]}>
                        <View style={styles.floatContainerNoPadding}>
                            <Text style={[styles.headerSubTitleBallon, styles.itemDetail]}>Maibi Navarro</Text>
                            <Text style={{fontWeight:'bold', paddingTop: 13}}>x</Text>
                        </View>
                    </View>
                </View>

                <Button
                    title="Incluir amigo"
                    color="#03AFBA"
                />

                <Text style={[styles.headerTitle, styles.itemDetail]}>Peça comidas e bebidas pela sua comanda virtual sem sair do app. Simples assim.</Text>

                {/* Menu */}
                <View>
                    <Text style={styles.sectionTitle}>Promoção</Text>
                    <FlatList
                    data={this.state.promoDataSource}
                    renderItem={({item}) =>
                        <TouchableOpacity 
                        onPress={() => {
                            this.props.navigation.navigate('FoodDescription', {food: item});
                        }}
                        >
                            <View style={styles.floatContainer} >
                                <View style={{width:'70%'}}>
                                    <Text style={styles.itemTitle}>{item.title}</Text>
                                    <Text style={styles.itemDetail}>{item.detail}</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.itemDetail}>R${item.promotionValue}</Text><Text style={styles.oldValue}> R${item.defaultValue}</Text>
                                    </View>
                                </View>
                                <Image source = {{ uri: item.uri }} style={styles.imageView} /> 
                            </View>
                        </TouchableOpacity>
                    }
                    />
                
                    <Text style={styles.sectionTitle}>Pratos</Text>
                    <FlatList
                        data={this.state.platesDataSource}
                        renderItem={({item}) => 
                        <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('FoodDescription', {food: item});
                        }}
                        >
                        <View style={styles.floatContainer} >
                            <View style={{width:'70%'}}>
                                <Text style={styles.itemTitle}>{item.title}</Text>
                                <Text style={styles.itemDetail}>{item.detail}</Text>
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.itemDetail}>R${item.promotionValue}</Text><Text style={styles.oldValue}> R${item.defaultValue}</Text>
                                </View>
                            </View>
                            <Image source = {{ uri: item.uri }} style={styles.imageView} /> 
                        </View>
                        </TouchableOpacity>
                        }
                    />
                </View>
                <View style={styles.bottomOval}>
                    <View style={styles.floatContainer}>
                        <Text style={styles.itemTitle}>Sua conta</Text>    
                        <View style={{flex: 1}}>
                            <Text style={[styles.itemTitle, {textAlign: 'right'}]}>R$67,50</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}