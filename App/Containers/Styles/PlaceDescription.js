import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics, ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  scrollview: {
    marginBottom: 56
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 20
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5
  },
  flex: {
    flex: 1
  },
  name: {
    ...Fonts.style.h1,
    color: '#03AFBA',
    flex: 1
  },
  wish: {
    color: Colors.purple,
    marginRight: 10
  },
  favorite: {
    color: Colors.red
  },
  reviewContainer: {
    alignItems: 'flex-start'
  },
  recommended: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  recommendedIcon: {
    marginRight: 7,
    color: '#03AFBA'
  },
  recommendedText: {
    color: '#000',
    fontSize: Fonts.size.regular
  },
  label: {
    color: '#000',
    fontSize: Fonts.size.small
  },
  status: {
    fontFamily: Fonts.type.bold
  },
  flux: {
    color: '#03AFBA',
    fontFamily: Fonts.type.bold
  },
  description: {
    color: '#000',
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base
  },
  image: {
    backgroundColor: 'transparent',
    width: Metrics.screenWidth
  },
  pagination: {
    backgroundColor: 'transparent',
    bottom: -24,
    left: 0,
    marginLeft: 'auto',
    marginRight: 'auto',
    position: 'absolute',
    right: 0
  },
  paginationDots: {
    backgroundColor: Colors.lightest,
    borderRadius: 8,
    height: 8,
    width: 8
  },
  button: {
    backgroundColor: '#03AFBA',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 5
  },
  buttonText: {
    color: '#FFF',
    fontSize: Fonts.size.medium
  },
  operation: {
    backgroundColor: '#D9FFD6',
    padding: 25,
    marginTop: 10
  },
  operationTitle: {
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
    color: '#03AFBA'
  },
  operationText: {
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    color: '#000'
  },
  locationIcon: {
    color: '#03AFBA',
    marginRight: 5
  },
  locationText: {
    color: '#000',
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base
  },
  checkinContainer: {
    position: 'absolute',
    bottom: 0,
    height: 56,
    backgroundColor: '#03AFBA',
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkinText: {
    color: '#D7E4F4',
    fontSize: Fonts.size.big,
    fontFamily: Fonts.type.base
  }
})
