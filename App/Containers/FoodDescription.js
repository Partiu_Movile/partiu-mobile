import React, { Component } from 'react';

import {
    View,
    Text,
    Image,
    TouchableOpacity,
    InputText,
    StyleSheet
} from 'react-native';

export default class FoodDescription extends Component {
    constructor() {
        super();
        this.state = {
            quantity: 1
        }
    }

    minus() {
        const qtt  = this.props.quantity - 1;
        this.setState({ quantity: qtt });
    }

    plus() {
        const qtt  = this.props.quantity + 1;
        this.setState({ quantity: qtt });
    }

    render() {
        const food = this.props.navigation.getParam('food', {});
        return (
            <View>
                <Text styles={{fontSize: 20}}>{food.title}</Text>
                <Text>{food.detail}</Text>
                <Image source={require('../../assets/img/food.jpeg')}/>
                <View>
                    <TouchableOpacity onPress={this.minus.bind(this)}>
                        <Text>-</Text>
                    </TouchableOpacity>
                    <InputText style={styles.input}
                        onChangeText={(quantity) => this.setState({ quantity })}
                        value={this.state.quantity} />
                    <TouchableOpacity onPress={this.plus.bind(this)}>
                        <Text>+</Text>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({})